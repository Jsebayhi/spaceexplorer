#include "ClockDiffHandler.class.hpp"
#include <iostream>
#include <sstream>

//class ClockDiffHandler

//  CONSTRUCTORS
//   public
	ClockDiffHandler::ClockDiffHandler(int clockDiffTrigger, int clockDiffMultiplicator){
		this->_objectDefaultValues();
		this->_clockDiffTrigger = clockDiffTrigger;
		if (clockDiffTrigger > 0){
			this->_clockDiffTrigger = clockDiffTrigger;
		} else {
			throw "clockDiffTrigger is inferior or equal to 0.";
		}
		if (clockDiffMultiplicator > 0){
			this->_clockDiffMultiplicator = clockDiffMultiplicator;
		} else {
			throw "clockDiffMultiplicator is inferior or equal to 0.";
		}
	}

	ClockDiffHandler::ClockDiffHandler(ClockDiffHandler const & model){
		*this = model;
	}
//   protected
//   private
	ClockDiffHandler::ClockDiffHandler(void){
		this->_objectDefaultValues();
	}

//  DESTRUCTORS
//   public
	ClockDiffHandler::~ClockDiffHandler(void){}
//   protected
//   private

//  OPERATOR
//   public
	ClockDiffHandler & ClockDiffHandler::operator=(ClockDiffHandler const & rhs){
		if (this != &rhs){
			this->_clockNow = rhs._clockNow;
			this->_clockRef = rhs._clockRef;
			this->_clockDiffTrigger = rhs._clockDiffTrigger;
			this->_clockDiffMultiplicator = rhs._clockDiffMultiplicator;
		}
		return (*this);
	}
//   protected
//   private

//	METHODS
//   public
	bool	ClockDiffHandler::clockDiffTriggerCheck(){
		const bool	Triggered = true;
		const bool	NotTriggered = !Triggered;

		this->_actualizeClockNow();
		if (this->_clockDiffTrigger * this->_clockDiffMultiplicator <= this->_clockNow - this->_clockRef){
			this->_actualizeClockRef();
			return Triggered;
		}
		return NotTriggered;
	}

	bool	ClockDiffHandler::clockDiffMultiplicatorAdd(int n){
		const bool	ChangeApplied = true;
		const bool	ChangeNotApplied = false;

		int	newClockDiffMultiplicator = this->_clockDiffMultiplicator + n;
		if (newClockDiffMultiplicator > 0){
			this->_clockDiffMultiplicator = newClockDiffMultiplicator;
			return ChangeApplied;
		}
		return ChangeNotApplied;
	}
//   protected
	void	ClockDiffHandler::_actualizeClockNow(){
		this->_clockNow = clock();
	}

	void	ClockDiffHandler::_actualizeClockRef(){
		this->_clockRef = this->_clockNow;
	}
//   private
	void	ClockDiffHandler::_objectDefaultValues(void){
		this->_actualizeClockNow();
		this->_actualizeClockRef();
	}

//  UTILITIES
//   public
	std::string	ClockDiffHandler::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
			ss << "_clockNow: " << _clockNow << separator
				<< "_clockRef: " << _clockRef << separator
				<< "_clockDiffTrigger: " << _clockDiffTrigger << separator
				<< "_clockDiffMultiplicator: " << _clockDiffMultiplicator
		;
		return ss.str();
	}
	std::string	ClockDiffHandler::formatToString(std::string separator) const{return ToString::formatToString(separator);}
	std::string	ClockDiffHandler::to_string(void) const{return ToString::to_string();}
	std::string	ClockDiffHandler::to_json(void) const{return ToString::to_json();}
//   protected
//   private

//  GETTER and SETTER
//   public
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, ClockDiffHandler const & rhs){
	o << rhs.to_string();
	return o;
}