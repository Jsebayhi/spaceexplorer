#include "TimeDiffHandler.class.hpp"
#include <iostream>
#include <sstream>

//class TimeDiffHandler

//  STATIC METHODS
//   public
//   protected
	int 	TimeDiffHandler::_diff_ms(timeval t1, timeval t2)//TODO: explicit cast
	{
	    return (((t1.tv_sec - t2.tv_sec) * 1000000) + (t1.tv_usec - t2.tv_usec))/1000;
	}
//   private

//  CONSTRUCTORS
//   public
	TimeDiffHandler::TimeDiffHandler(double timeDiffTrigger){
		std::cerr << "debug\n";
		this->_objectDefaultValues();
		std::cerr << "debug\n";
		if (timeDiffTrigger > 0){
			this->_timeDiffTrigger = timeDiffTrigger;
		} else {
			throw "timeDiffTrigger is inferior or equal to 0.";
		}
		std::cerr << "debug\n";
	}
	TimeDiffHandler::TimeDiffHandler(TimeDiffHandler const & model){
		*this = model;
	}
//   protected
//   private
	TimeDiffHandler::TimeDiffHandler(void){
		this->_objectDefaultValues();
	}

//  DESTRUCTORS
//   public
	TimeDiffHandler::~TimeDiffHandler(void){}
//   protected
//   private

//  OPERATOR
//   public
	TimeDiffHandler & TimeDiffHandler::operator=(TimeDiffHandler const & rhs){
		if (this != &rhs){
			this->_timeNow = rhs._timeNow;
			this->_timeRef = rhs._timeRef;
			this->_timeDiffTrigger = rhs._timeDiffTrigger;
		}
		return (*this);
	}
//   protected
//   private

//	METHODS
//   public
	bool	TimeDiffHandler::timeDiffTriggerCheck(){		
		const bool	Triggered = true;
		const bool	NotTriggered = !Triggered;

		this->_actualizeTimeNow();
		if (TimeDiffHandler::_diff_ms(this->_timeNow, this->_timeRef)){
			this->_actualizeTimeRef();
			return Triggered;
		}
		return NotTriggered;
	}
//   protected
	void	TimeDiffHandler::_actualizeTimeNow(){
		gettimeofday(&(this->_timeNow), NULL);
	}
	void	TimeDiffHandler::_actualizeTimeRef(){
		this->_timeRef = this->_timeNow;
	}
//   private
	void	TimeDiffHandler::_objectDefaultValues(void){
		this->_actualizeTimeNow();
		this->_actualizeTimeRef();
	}

//  UTILITIES
//   public
	std::string	TimeDiffHandler::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
			ss << "_timeNow: " << "this->_timeNow (undisplayable)" << separator
				<< "_timeRef: " << "this->_timeRef (undisplayable)" << separator
				<< "_timeDiffTrigger: " << this->_timeDiffTrigger << separator
		;
		return ss.str();
	}
	std::string	TimeDiffHandler::formatToString(std::string separator) const{return ToString::formatToString(separator);}
	std::string	TimeDiffHandler::to_string(void) const{return ToString::to_string();}
	std::string	TimeDiffHandler::to_json(void) const{return ToString::to_json();}
//   protected
//   private

//  GETTER and SETTER
//   public
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, TimeDiffHandler const & rhs){
	o << rhs.to_string();
	return o;
}