#ifndef POINT2D_CLASS_HPP
# define POINT2D_CLASS_HPP
# include <iostream>

class Point2D {
public:
	int		x;
	int		y;

	Point2D(void);
	Point2D(int x, int y);
	Point2D(Point2D const & model);
	~Point2D(void);

//  OPERATOR

	Point2D & operator=(Point2D const & rhs);
	bool operator==(Point2D const & rhs) const;

//	METHODS

//	UTILITIES

	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;

//	GETTER and SETTER

private:
	void 	_objectDefaultValues(void);
};

std::ostream & operator<<(std::ostream & o, Point2D const & rhs);

#endif