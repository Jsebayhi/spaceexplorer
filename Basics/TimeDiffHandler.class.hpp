#ifndef TIMEDIFFHANDLER_CLASS_HPP
# define TIMEDIFFHANDLER_CLASS_HPP
# include "ToString.class.hpp"
# include <iostream>
# include <sys/time.h>

class TimeDiffHandler: ToString{
//  STATIC METHOD
//public:
protected:
	static int	_diff_ms(timeval t1, timeval t2);
//private:

//  VARIABLE
//public:
//protected:
private:
	timeval	_timeNow;
	timeval	_timeRef;
	double	_timeDiffTrigger;

//  CONSTRUCTORS
public:
	TimeDiffHandler(double timeDiffTrigger);
	TimeDiffHandler(TimeDiffHandler const & model);
//protected:
private:
	TimeDiffHandler(void);

//  DESTRUCTORS
public:
	~TimeDiffHandler(void);
//protected:
//private:

//  OPERATOR
public:
	TimeDiffHandler & operator=(TimeDiffHandler const & rhs);
//protected:
//private:

//	METHODS
public:
	bool	timeDiffTriggerCheck();
protected:
	void	_actualizeTimeNow();
	void	_actualizeTimeRef();
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	/* Allow to call this function to format either in inline string or as json by specifying the separator */
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, TimeDiffHandler const & rhs);

#endif