#include "Point2D.class.hpp"
#include <iostream>
#include <sstream>

//class Point2D
// public

	Point2D::Point2D(void){
		this->_objectDefaultValues();
	}

	Point2D::Point2D(int x, int y){
		this->_objectDefaultValues();
		this->x = x;
		this->y = y;
	}

	Point2D::Point2D(Point2D const & model){
		*this = model;
	}

	Point2D::~Point2D(void){}

//  OPERATOR

	Point2D & Point2D::operator=(Point2D const & rhs){
		if (this != &rhs){
			this->x = rhs.x;
			this->y = rhs.y;
		}
		return (*this);
	}

	bool Point2D::operator==(Point2D const & rhs) const{
		const bool	match = true;

		if (this->x == rhs.x && this->y == rhs.y)
			return match;
		return !match;
	}

//	METHODS

//  UTILITIES

	std::string	Point2D::formatToString(std::string separator) const{
		std::stringstream ss;
		ss << "Address: " << this << separator
			<< this->x << separator
			<< this->y
		;
		return ss.str();
	}

	std::string	Point2D::to_string(void) const{
		const std::string	objectOpening = "{ ";
		const std::string	objectEnding = " }";
		const std::string	separator = ", ";

		std::stringstream ss;
		ss << objectOpening
			<< this->formatToString(separator)
			<< objectEnding 
		;
		return ss.str();
	}

	std::string	Point2D::to_json(void) const{
		const std::string	objectOpening = "{\n\t";
		const std::string	objectEnding = "\n}\n";
		const std::string	separator = "\n\t ";

		std::stringstream ss;
		ss << objectOpening
			<< this->formatToString(separator)
			<< objectEnding 
		;
		return ss.str();
	}

//  GETTER and SETTER

// private
	void	Point2D::_objectDefaultValues(void){
		this->x = 0;
		this->y = 0;
	}

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, Point2D const & rhs){
	o << rhs.to_string();
	return o;
}