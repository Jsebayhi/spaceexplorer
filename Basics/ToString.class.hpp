#ifndef TOSTRING_CLASS_HPP
# define TOSTRING_CLASS_HPP
# include <iostream>

class ToString {
//  VARIABLE
//public:
//protected:
//private:


//  CONSTRUCTORS
public:
	ToString(void);
	ToString(ToString const & model);

//  DESTRUCTORS
public:
	virtual ~ToString(void);

//  OPERATOR
public:
	ToString & operator=(ToString const & rhs);

//	METHODS
//public:
//protected:
//private

//	UTILITIES
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	/* Allow to call this function to format either in inline string or as json by specifying the separator */
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, ToString const & rhs);

#endif