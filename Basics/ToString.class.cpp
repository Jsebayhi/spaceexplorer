#include "ToString.class.hpp"
#include <iostream>
#include <sstream>

//class ToString

//  CONSTRUCTORS
//   public
	ToString::ToString(void){}

	ToString::ToString(ToString const & model){
		*this = model;
	}

//  DESTRUCTORS
//   public
	ToString::~ToString(void){}

//  OPERATOR
//   public
	ToString & ToString::operator=(ToString const & rhs){
		if (this != &rhs){
		}
		return (*this);
	}

//	METHODS
//   public
//   protected
//   private

//  UTILITIES
//   public
//   protected
//   private

	std::string	ToString::formatAttributsToString(__attribute__((unused)) std::string separator) const{
		std::stringstream ss;
			ss << ""
		;
		return ss.str();
	}

	std::string	ToString::formatToString(std::string separator) const{
		std::stringstream ss;
		ss << "Address: " << this << this->formatAttributsToString(separator);
		return ss.str();
	}

	std::string	ToString::to_string(void) const{
		const std::string	objectOpening = "{ ";
		const std::string	objectEnding = " }";
		const std::string	separator = ", ";

		std::stringstream ss;
		ss << objectOpening
			<< this->formatToString(separator)
			<< objectEnding 
		;
		return ss.str();
	}

	std::string	ToString::to_json(void) const{
		const std::string	objectOpening = "{\n\t";
		const std::string	objectEnding = "\n}\n";
		const std::string	separator = "\n\t ";

		std::stringstream ss;
		ss << objectOpening
			<< this->formatToString(separator)
			<< objectEnding 
		;
		return ss.str();
	}

//  GETTER and SETTER
//   public
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, ToString const & rhs){
	o << rhs.to_string();
	return o;
}