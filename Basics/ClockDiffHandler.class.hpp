#ifndef CLOCKDIFFHANDLER_CLASS_HPP
# define CLOCKDIFFHANDLER_CLASS_HPP
# include "ToString.class.hpp"
# include <iostream>
# include <time.h>

class ClockDiffHandler: ToString{
//  GLOBAL METHOD
//public:
//protected:
//private:

//  VARIABLE
//public:
//protected:
private:
	clock_t	_clockNow;
	clock_t	_clockRef;
	int		_clockDiffTrigger;
	int		_clockDiffMultiplicator;

//  CONSTRUCTORS
public:
	ClockDiffHandler(int clockDiffTrigger, int clockDiffMultiplicator);
	ClockDiffHandler(ClockDiffHandler const & model);
//protected:
private:
	ClockDiffHandler(void);

//  DESTRUCTORS
public:
	~ClockDiffHandler(void);
//protected:
//private:

//  OPERATOR
public:
	ClockDiffHandler & operator=(ClockDiffHandler const & rhs);
//protected:
//private:

//	METHODS
public:
	bool	clockDiffMultiplicatorAdd(int n);
	bool	clockDiffTriggerCheck();
protected:
	void	_actualizeClockNow();
	void	_actualizeClockRef();
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	/* Allow to call this function to format either in inline string or as json by specifying the separator */
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, ClockDiffHandler const & rhs);

#endif