#ifndef GAME_CLASS_HPP
# define GAME_CLASS_HPP
# include "ToString.class.hpp"
# include "Point2D.class.hpp"
# include "DisplayManager.class.hpp"
# include "GameEntity.class.hpp"
# include "Actor.class.hpp"
# include "ActorList.class.hpp"
# include <iostream>
# include <vector>

class Game: public ToString {
//  VARIABLE
public:
	static const bool			GameStatusRunning = true;
	static const bool			GameStatusOver = false;
//protected:
private:
	bool						_gameStatus;
	DisplayManager				_displayManager;
	Point2D						_screenRefPoint;
	GameEntity					*_screenRefGameEntity;
	ActorList					_gameEntityList;

public:
	Game(void);
	Game(Game const & model);
	~Game(void);

//  OPERATOR
public:
	Game & operator=(Game const & rhs);
//protected:
//private:

//	METHODS
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	int		run();
//protected:
private:
	void	_updateScreenRefPoint();
	void	_updateScreen(Actor *actor);//TODO: remove player from there.
	void	_actorActs();
	void	_gameEntityListRemoveDeadActor();
	void	_checkEntityCollision(Actor *ge);

//	UTILITIES
//public:
//protected:
//private:


//	GETTER and SETTER
//public:
//protected:
private:
	void 	_objectDefaultValues(void);
};

std::ostream & operator<<(std::ostream & o, Game const & rhs);

#endif