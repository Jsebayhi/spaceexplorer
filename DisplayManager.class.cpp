#include "DisplayManager.class.hpp"
#include <iostream>
#include <sstream>

	int	PosXAbsoluteToScreenRef(int screenSizeX, int screenRefX, int posX){
		return (posX - screenRefX + screenSizeX / 2);
	}//TODO: make a method out of it

	int	PosYAbsoluteToScreenRef(int screenSizeY, int screenRefY, int posY){
		return (posY - screenRefY + screenSizeY / 2);	
	}//TODO: make a method out of it

//class DisplayManager
// public

	DisplayManager::DisplayManager(void){
	}

	DisplayManager::DisplayManager(DisplayManager const & model){
		*this = model;
	}

	DisplayManager::~DisplayManager(void){}

//  OPERATOR

	DisplayManager & DisplayManager::operator=(DisplayManager const & rhs){
		if (this != &rhs){
		}
		return (*this);
	}

//	METHODS

	bool	DisplayManager::init(){
		const bool	Success = true;
		
		initscr();
		raw();
		noecho();
		keypad(stdscr, 1);
		curs_set(0);
		nodelay(stdscr, TRUE);

		getmaxyx(stdscr, this->_screenSize.y, this->_screenSize.x);

		int	mainWindowScreenSizeX = this->_screenSize.x;
		int	mainWindowScreenSizeY = this->_screenSize.y;

		if (mainWindowScreenSizeX % 2){
			mainWindowScreenSizeX -= 1;
		}
		if (mainWindowScreenSizeY % 2){
			mainWindowScreenSizeY -= 1;
		}
		this->_mainWindow = newwin(mainWindowScreenSizeY, mainWindowScreenSizeX, 0, 0);

		return Success;
	}

	bool	DisplayManager::end(){
		const bool	Success = true;
		
		endwin();
		return Success;
	}

	bool	DisplayManager::winClear(){
		const bool	Success = true;

		wclear(this->_mainWindow);
		return Success;
	}

	bool	DisplayManager::winRefresh(){
		const bool	Success = true;

		wrefresh(this->_mainWindow);
		return Success;
	}

	bool	DisplayManager::displayPlaceableToWindow(Placeable *placeable){
		const bool	Success = true;

		Point2D	position;
		Point2D	mainWindowScreenSize;

		getmaxyx(this->_mainWindow, mainWindowScreenSize.y, mainWindowScreenSize.x);

		position.x = PosXAbsoluteToScreenRef(mainWindowScreenSize.x, this->screenRefPoint.x, placeable->getPos().x);
		position.y = PosYAbsoluteToScreenRef(mainWindowScreenSize.y, this->screenRefPoint.y, placeable->getPos().y);

		displayTextToWindow("#", position);
		return Success;
	}

	bool	DisplayManager::displayTextToWindow(std::string text, Point2D position){
		const bool	Success = true;

		mvwprintw(this->_mainWindow, position.y, position.x, text.c_str());
		return Success;
	}

//  UTILITIES

	std::string	DisplayManager::formatToString(std::string separator) const{
		std::stringstream ss;
		ss << "Address: " << this << separator
			<< "screenRefPoint: " << this->screenRefPoint
		;
		return ss.str();
	}

	std::string	DisplayManager::to_string(void) const{
		const std::string	objectOpening = "{ ";
		const std::string	objectEnding = " }";
		const std::string	separator = ", ";

		std::stringstream ss;
		ss << objectOpening
			<< this->formatToString(separator)
			<< objectEnding 
		;
		return ss.str();
	}

	std::string	DisplayManager::to_json(void) const{
		const std::string	objectOpening = "{\n\t";
		const std::string	objectEnding = "\n}\n";
		const std::string	separator = "\n\t ";

		std::stringstream ss;
		ss << objectOpening
			<< this->formatToString(separator)
			<< objectEnding 
		;
		return ss.str();
	}

//  GETTER and SETTER

// private
	void	DisplayManager::_objectDefaultValues(void){}

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, DisplayManager const & rhs){
	o << rhs.to_string();
	return o;
}