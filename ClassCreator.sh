CLASS_FILE_CPP_SUFFIX=".class.cpp"
CLASS_FILE_HPP_SUFFIX=".class.hpp"
CLASS_FILE_HPP_GUARD_SUFFIX="_CLASS_HPP"

CLASS_NAME="$1"
CLASS_NAME_UPPER_CASE="${CLASS_NAME^^}"

# Futur Class File Parameters

FILE_CPP="${CLASS_NAME}${CLASS_FILE_CPP_SUFFIX}"
FILE_HPP="${CLASS_NAME}${CLASS_FILE_HPP_SUFFIX}"
FILE_HPP_DEFINE_GUARD="${CLASS_NAME_UPPER_CASE}${CLASS_FILE_HPP_GUARD_SUFFIX}"

# Template File

TEMPLATE_FILE_CPP_PATH="./"
TEMPLATE_FILE_CPP_NAME="canonical.tmpl.cpp"
TEMPLATE_FILE_CPP="${TEMPLATE_FILE_CPP_PATH}${TEMPLATE_FILE_CPP_NAME}"

TEMPLATE_FILE_HPP_PATH="./"
TEMPLATE_FILE_HPP_NAME="canonical.tmpl.hpp"
TEMPLATE_FILE_HPP="${TEMPLATE_FILE_HPP_PATH}${TEMPLATE_FILE_HPP_NAME}"

TEMPLATE_CLASS_NAME="Canonical"
TEMPLATE_HPP_DEFINE_GUARD="CANONICAL_HPP"

#
# NEW CLASS BUILDING PROCESS
#

# HPP FILE

echo "class.hpp: Creating new class based on template."
cp "${TEMPLATE_FILE_HPP}" "${FILE_HPP}"

echo "class.hpp: Adapting hpp double inclusion guard."
sed -ie "s/${TEMPLATE_HPP_DEFINE_GUARD}/${FILE_HPP_DEFINE_GUARD}/g" "${FILE_HPP}"

echo "class.hpp: Replacing default class by name by specified one."
sed -ie "s/${TEMPLATE_CLASS_NAME}/${CLASS_NAME}/g" "${FILE_HPP}"

echo "class.hpp: Deleting sed backup."
rm "${FILE_HPP}e"

# CPP FILE

echo "class.cpp: Creating new class based on template."
cp "${TEMPLATE_FILE_CPP}" "${FILE_CPP}"

echo "class.cpp: Replacing default class.hpp fileName."
sed -ie "s/${TEMPLATE_FILE_HPP_NAME}/${FILE_HPP}/g" "${FILE_CPP}"

echo "class.cpp: Replacing default class name by specified one."
sed -ie "s/${TEMPLATE_CLASS_NAME}/${CLASS_NAME}/g" "${FILE_CPP}"

echo "class.cpp: Deleting sed backup."
rm "${FILE_CPP}e"