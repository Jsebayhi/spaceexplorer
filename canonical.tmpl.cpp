#include "canonical.tmpl.hpp"
#include <iostream>
#include <sstream>

//class Canonical

//  STATIC METHOD
//public
//protected
//private

//  CONSTRUCTORS
//   public
	Canonical::Canonical(void){
		this->_objectDefaultValues();
	}

	Canonical::Canonical(Canonical const & model){
		*this = model;
	}
//   protected
//   private

//  DESTRUCTORS
//   public
	Canonical::~Canonical(void){}
//   protected
//   private

//  OPERATOR
//   public
	Canonical & Canonical::operator=(Canonical const & rhs){
		if (this != &rhs){
		}
		return (*this);
	}
//   protected
//   private

//	METHODS
//   public
//   protected
//   private
	void	Canonical::_objectDefaultValues(void){}

//  UTILITIES
//   public
	std::string	Canonical::formatAttributsToString(__attribute__((unused)) std::string separator) const{
		std::stringstream ss;
			ss << ""
		;
		return ss.str();
	}
	std::string	Canonical::formatToString(std::string separator) const{return ToString::formatToString(separator);}
	std::string	Canonical::to_string(void) const{return ToString::to_string();}
	std::string	Canonical::to_json(void) const{return ToString::to_json();}
//   protected
//   private

//  GETTER and SETTER
//   public
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, Canonical const & rhs){
	o << rhs.to_string();
	return o;
}