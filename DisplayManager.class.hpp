#ifndef DISPLAYMANAGER_CLASS_HPP
# define DISPLAYMANAGER_CLASS_HPP
# include "Point2D.class.hpp"
# include "Placeable.interface.hpp"
# include "GameEntity.class.hpp"
# include <iostream>
# include <ncurses.h>

class DisplayManager {
public:
	Point2D	screenRefPoint;

	DisplayManager(void);
	DisplayManager(DisplayManager const & model);
	~DisplayManager(void);

//  OPERATOR

	DisplayManager & operator=(DisplayManager const & rhs);

//	METHODS

	bool	init();
	bool	end();
	bool	winClear();
	bool	winRefresh();
	bool	displayPlaceableToWindow(Placeable *placeable);
	bool	displayTextToWindow(std::string text, Point2D position);

//	UTILITIES

	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;

//	GETTER and SETTER
private:
	//DisplayLib	displayLib;
	WINDOW	*_mainWindow;//TODO: switch to ID
	Point2D	_screenSize;
	void 	_objectDefaultValues(void);
};

std::ostream & operator<<(std::ostream & o, DisplayManager const & rhs);

#endif