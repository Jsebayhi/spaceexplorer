#################################
# PROJECT GENERAL CONFIGURATION #
#################################

include Makefile.conf

NAME = program.exe

# FallBack
##########

CC ?= clang++
CFLAGS ?= -Wall -Wextra -Werror

# Const
#######

SRC_PATH = ./

OBJ_PATH = ./.obj/

SRC_NAME = main.cpp\
			DisplayManager.class.cpp\
			Game.class.cpp\
			
OBJ_NAME = $(SRC_NAME:.cpp=.o)

INC_PATH = ./Basics/\
			./GameEntities/\

LIB_PATH = ./Basics\
			./GameEntities/\

LIB_NAME = -l_basics -l_gameEntities

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
LIB = $(addprefix -L,$(LIB_PATH))
INC = $(addprefix -I,$(INC_PATH))

#####################
# COMMAND - DEFAULT #
#####################

all: lib_make $(NAME)

###################
# COMMAND - OTHER #
###################

$(NAME): $(OBJ)
	$(CC) -lncurses $(CFLAGS) $(OBJ) $(LIB) $(LIB_NAME) $(INC)  -o $(NAME)

$(OBJ_PATH)%.o:$(SRC_PATH)%.cpp
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	rm -rfv $(OBJ_PATH)

fclean: lib_fclean clean
	rm -fv $(NAME)

re: fclean lib_fclean all

lib_make:
	for lib in $(LIB_PATH); do \
	make -C $$lib ;\
	done

lib_clean:
	for lib in $(LIB_PATH); do \
	make clean -C $$lib ;\
	done

lib_fclean:
	for lib in $(LIB_PATH); do \
	make fclean -C $$lib ;\
	done

lib_re:
	for lib in $(LIB_PATH); do \
	make re -C $$lib ;\
	done
