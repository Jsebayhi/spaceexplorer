#include "Game.class.hpp"
#include "Player.class.hpp"
#include "GameEntity.class.hpp"
#include <iostream>

int	test(){
	{
		GameEntity	gE;

		std::cout << gE.to_json();
	}
	{
		Player	player;

		std::cout << player.to_json();
	}
	return (0);
}

int	main(){
	Game	game;

	game.run();

	test();
}