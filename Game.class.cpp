#include "Game.class.hpp"
#include "GameEntity.class.hpp"
#include "ClockDiffHandler.class.hpp"
#include "TimeDiffHandler.class.hpp"
#include "Player.class.hpp"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <algorithm>

//class Game
// public

	Game::Game(void){
		this->_objectDefaultValues();
	}

	Game::Game(Game const & model){
		*this = model;
	}

	Game::~Game(void){}

//  OPERATOR

	Game & Game::operator=(Game const & rhs){
		if (this != &rhs){
		}
		return (*this);
	}

// TODO: move to time diff handling class

//	METHODS
	int		Game::run(){

		// GAME INITIALIZATION

		Player	*player = new Player(&(this->_gameEntityList), Point2D(4, 0));
		this->_gameEntityList.registerActor(player);
		this->_gameEntityList.registerActor(new Actor(&(this->_gameEntityList), Point2D(-8, 4), Point2D(0, 0)));
		this->_gameEntityList.registerActor(new Actor(&(this->_gameEntityList), Point2D(8, 4), Point2D(-1, 0)));
		this->_gameEntityList.registerActor(new Actor(&(this->_gameEntityList), Point2D(-8, 5), Point2D(0, 0)));
		this->_gameEntityList.registerActor(new Actor(&(this->_gameEntityList), Point2D(8, 5), Point2D(0, 0)));
		this->_gameEntityList.registerActor(new Actor(&(this->_gameEntityList), Point2D(-8, 2), Point2D(0, 0)));
		this->_gameEntityList.registerActor(new Actor(&(this->_gameEntityList), Point2D(9, 2), Point2D(-1, 0)));

		this->_screenRefGameEntity = player;
		int	key = -1;

		ClockDiffHandler	*GameActionsClockHandler = NULL;
		TimeDiffHandler		*GameDisplayTimeHandler = NULL;
		
		try {//TODO remove the catch so that the exception can behave as one and create a standard exception for the project
			GameActionsClockHandler = new ClockDiffHandler(100000, 1);
			GameDisplayTimeHandler = new TimeDiffHandler(1.0 / 24.0);
			this->_gameStatus = Game::GameStatusRunning;
		} catch(const char *e){
			std::cerr << e << "\n";
			this->_gameStatus = Game::GameStatusOver;
		}

		this->_displayManager.init();

		// GAME

		while(this->_gameStatus == Game::GameStatusRunning){
			if (GameActionsClockHandler->clockDiffTriggerCheck()){
				this->_actorActs();
				this->_gameEntityListRemoveDeadActor();
				if (Alive::isDead(player)){
					this->_gameStatus = Game::GameStatusOver;
				}
			}
			if (GameDisplayTimeHandler->timeDiffTriggerCheck()){
				this->_updateScreen(player);
			}
			{// Make a controller function which handle multi player
				const char	CKey_GameExit = 'l';
				const char	CKey_GameSpeedIncrease = 'p';
				const char	CKey_GameSpeedDecrease = 'm';
				const char	CKey_PlayerSpeedYDecrease = 'z';
				const char	CKey_PlayerSpeedYIncrease = 's';
				const char	CKey_PlayerSpeedXDecrease = 'q';
				const char	CKey_PlayerSpeedXIncrease = 'd';
				const char	CKey_PlayerFireWeapon = 'f';

				if (-1 != (key = getch())){
					if (CKey_GameExit == key)
						break;
					else if (CKey_GameSpeedIncrease == key)
						GameActionsClockHandler->clockDiffMultiplicatorAdd(-1);
					else if (CKey_GameSpeedDecrease == key)
						GameActionsClockHandler->clockDiffMultiplicatorAdd(1);
					else if (CKey_PlayerSpeedYDecrease == key)
						player->SpeedYDecrease();
					else if (CKey_PlayerSpeedYIncrease == key)
						player->SpeedYIncrease();
					else if (CKey_PlayerSpeedXDecrease == key)
						player->SpeedXDecrease();
					else if (CKey_PlayerSpeedXIncrease == key)
						player->SpeedXIncrease();
					else if (CKey_PlayerFireWeapon == key)
						player->FireWeapon();
				}
			}
		}

		// GAME END

		while (-1 == getch())
			;
		this->_displayManager.end();
		return 0;
	}

//   private
	void	Game::_objectDefaultValues(void){
		this->_gameStatus = Game::GameStatusOver;
	}

	void	Game::_checkEntityCollision(Actor	*ge){
		int	_gameEntityListSize = this->_gameEntityList.getSize();
		for(int i = 0; i < _gameEntityListSize; i++) {
			if (ge != this->_gameEntityList[i] && !Alive::isDead(ge)){
				if (this->_gameEntityList[i]->getPos() == ge->getPos()){
					this->_gameEntityList[i]->ApplyDamages(ge->getCollisionDamages());
					ge->ApplyDamages(this->_gameEntityList[i]->getCollisionDamages());
				}
			}
		}
	}

	void	Game::_actorActs(){
		std::cerr << "Actor acting:\n";
		int	_gameEntityListSize = this->_gameEntityList.getSize();
		for(int i = 0; i < _gameEntityListSize; i++) {
			this->_gameEntityList[i]->Act();
			//std::cerr << "\t" << (*it)->to_json() << "\n";
			this->_checkEntityCollision(this->_gameEntityList[i]);
		}
	}

	void	Game::_gameEntityListRemoveDeadActor(){
		this->_gameEntityList.removeDeadActor();
	}

	void	Game::_updateScreen(Actor *actor){
		this->_updateScreenRefPoint();
		this->_displayManager.winClear();
		int	_gameEntityListSize = this->_gameEntityList.getSize();
		for(int i = 0; i < _gameEntityListSize; i++) {
			this->_displayManager.displayPlaceableToWindow(this->_gameEntityList[i]);
		}
		{//TODO: create proper internal method
			// Display Info
			std::stringstream	ss;
			int					posY = 1;

			ss.str("");
			ss << "Screen ref: (x,y): " << this->_screenRefPoint.x << ", " << this->_screenRefPoint.y;
			this->_displayManager.displayTextToWindow(ss.str(), Point2D(1, posY++));
			
			ss.str("");
			ss  << "Actor pos (x,y): " << actor->getPos().x << ", " <<  actor->getPos().y;
			this->_displayManager.displayTextToWindow(ss.str(), Point2D(1, posY++));

			ss.str("");
			ss  << "Actor speed (x,y): " << actor->getSpeed().x << ", " <<  actor->getSpeed().y;
			this->_displayManager.displayTextToWindow(ss.str(), Point2D(1, posY++));
		}
		this->_displayManager.winRefresh();
	}

	void	Game::_updateScreenRefPoint(){
		int	SpeedFeelingPos = 8;

		this->_screenRefPoint.x = this->_screenRefGameEntity->getPos().x;
		this->_screenRefPoint.y = this->_screenRefGameEntity->getPos().y;
		
		// Add movement feeling to the viewer:
		if (abs(this->_screenRefGameEntity->getSpeed().x) < SpeedFeelingPos)
			this->_screenRefPoint.x -= this->_screenRefGameEntity->getSpeed().x;
		else if (this->_screenRefGameEntity->getSpeed().x < 0)
			this->_screenRefPoint.x += SpeedFeelingPos;
		else if (this->_screenRefGameEntity->getSpeed().x > 0)
			this->_screenRefPoint.x -= SpeedFeelingPos;

		if (abs(this->_screenRefGameEntity->getSpeed().y) < SpeedFeelingPos)
			this->_screenRefPoint.y -= this->_screenRefGameEntity->getSpeed().y;
		else if (this->_screenRefGameEntity->getSpeed().y < 0)
			this->_screenRefPoint.y += SpeedFeelingPos;
		else if (this->_screenRefGameEntity->getSpeed().y > 0)
			this->_screenRefPoint.y -= SpeedFeelingPos;

		this->_displayManager.screenRefPoint = this->_screenRefPoint;
	}

//  UTILITIES

	std::string	Game::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
		ss << separator
			<< "_displayManager: " << this->_displayManager << separator
			<< "_screenRefPoint: " << this->_screenRefPoint << separator
			<< "_screenRefGameEntity: " << this->_screenRefGameEntity
		;
		return ss.str();
	}

//  GETTER and SETTER

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, Game const & rhs){
	o << rhs.to_string();
	return o;
}