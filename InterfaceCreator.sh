INTERFACE_FILE_HPP_SUFFIX=".interface.hpp"
INTERFACE_FILE_HPP_GUARD_SUFFIX="_INTERFACE_HPP"

INTERFACE_NAME="$1"
INTERFACE_NAME_UPPER_CASE="${INTERFACE_NAME^^}"

# Futur Class File Parameters

FILE_HPP="${INTERFACE_NAME}${INTERFACE_FILE_HPP_SUFFIX}"
FILE_HPP_DEFINE_GUARD="${INTERFACE_NAME_UPPER_CASE}${INTERFACE_FILE_HPP_GUARD_SUFFIX}"

# Template File

TEMPLATE_FILE_HPP_PATH="./"
TEMPLATE_FILE_HPP_NAME="interface.tmpl.hpp"
TEMPLATE_FILE_HPP="${TEMPLATE_FILE_HPP_PATH}${TEMPLATE_FILE_HPP_NAME}"

TEMPLATE_INTERFACE_NAME="INAME"
TEMPLATE_HPP_DEFINE_GUARD="INTERFACE_HPP"

#
# NEW CLASS BUILDING PROCESS
#

# HPP FILE

echo "interface.hpp: Creating new interface based on template."
cp "${TEMPLATE_FILE_HPP}" "${FILE_HPP}"

echo "interface.hpp: Adapting hpp double inclusion guard."
sed -ie "s/${TEMPLATE_HPP_DEFINE_GUARD}/${FILE_HPP_DEFINE_GUARD}/g" "${FILE_HPP}"

echo "interface.hpp: Replacing default interface by name by specified one."
sed -ie "s/${TEMPLATE_INTERFACE_NAME}/${INTERFACE_NAME}/g" "${FILE_HPP}"

echo "interface.hpp: Deleting sed backup."
rm "${FILE_HPP}e"
