#include "Actor.class.hpp"
#include <iostream>
#include <sstream>

//class Actor

//  STATIC METHOD
//public
//protected
//private

//  CONSTRUCTORS
//   public
	Actor::Actor(void):  GameEntity(Point2D(0, 0), Point2D(0, 0)){
		this->_objectDefaultValues();
	}
	Actor::Actor(ActorListRestricted *actorList, Point2D position):  GameEntity(position, Point2D(0, 0)){
		this->_objectDefaultValues();
		this->_position = position;
		this->_actorList = actorList;
	}
	Actor::Actor(ActorListRestricted *actorList, Point2D position, Point2D speed): GameEntity(){
		this->_objectDefaultValues();
		this->_position = position;
		this->_speed = speed;
		this->_actorList = actorList;
	}
	Actor::Actor(Actor const & model){
		*this = model;
	}
//   protected
//   private

//  DESTRUCTORS
//   public
	Actor::~Actor(void){}
//   protected
//   private

//  OPERATOR
//   public
	Actor & Actor::operator=(Actor const & rhs){
		if (this != &rhs){
			GameEntity::operator=(rhs);
			Alive::operator=(rhs);
		}
		return (*this);
	}
//   protected
//   private

//	METHODS
//   public
//   protected
	bool Actor::_fireWeapon(){
		const bool	Success = true;

		Actor	*gift = new	Actor(this->_actorList, Point2D(this->_position.x, this->_position.y - 1), Point2D(0, this->_speed.y - 1));
		this->_actorList->registerActor(gift);
		gift->Act();
		return Success;
	}
//   private
	void	Actor::_objectDefaultValues(void){
		this->_actorList = NULL;
	}

//  UTILITIES
//   public
	std::string	Actor::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
			ss << GameEntity::formatAttributsToString(separator) << separator
				<< Alive::formatAttributsToString(separator)
		;
		return ss.str();
	}
	std::string	Actor::formatToString(std::string separator) const{return ToString::formatToString(separator);}
	std::string	Actor::to_string(void) const{return ToString::to_string();}
	std::string	Actor::to_json(void) const{return ToString::to_json();}
//   protected
//   private

//  GETTER and SETTER
//   public
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, Actor const & rhs){
	o << rhs.to_string();
	return o;
}