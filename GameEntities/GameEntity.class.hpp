#ifndef GAMEENTITY_CLASS_HPP
# define GAMEENTITY_CLASS_HPP
# include "ToString.class.hpp"
# include "Point2D.class.hpp"
# include "Placeable.interface.hpp"
# include <iostream>

class GameEntity: virtual public ToString, public Placeable{
public:
	GameEntity(void);
	GameEntity(Point2D position, Point2D Speed);
	GameEntity(GameEntity const & model);
	virtual ~GameEntity(void);

//  OPERATOR

	GameEntity & operator=(GameEntity const & rhs);

//	METHODS

	virtual void	Act();

//  UTILITIES

	virtual std::string	formatAttributsToString(std::string separator) const;

//  GETTER and SETTER
	Point2D	getPos() const;
	Point2D	getSpeed() const;

protected:
	Point2D		_position;
	Point2D		_speed;

private:
	void	_move();
	void 	_objectDefaultValues(void);
};

std::ostream & operator<<(std::ostream & o, GameEntity const & rhs);

#endif