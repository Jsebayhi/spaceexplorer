#ifndef ACTOR_CLASS_HPP
# define ACTOR_CLASS_HPP
# include "ToString.class.hpp"
# include "GameEntity.class.hpp"
# include "Alive.class.hpp"
# include "ActorListRestricted.interface.hpp"
# include <iostream>

class Actor: public GameEntity, public Alive {
//  STATIC METHOD
//public:
//protected:
//private:

//  VARIABLE
//public:
protected:
	ActorListRestricted	*_actorList;
//private:

//  CONSTRUCTORS
public:
	Actor(void);
	Actor(ActorListRestricted *actorList, Point2D position);
	Actor(ActorListRestricted *actorList, Point2D position, Point2D speed);
	Actor(Actor const & model);
//protected:
//private:

//  DESTRUCTORS
public:
	virtual ~Actor(void);
//protected:
//private:

//  OPERATOR
public:
	Actor & operator=(Actor const & rhs);
//protected:
//private:

//	METHODS
//public:
protected:
	virtual bool	_fireWeapon();
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	/* Allow to call this function to format either in inline string or as json by specifying the separator */
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, Actor const & rhs);

#endif