#ifndef PLACEABLE_INTERFACE_HPP
#define PLACEABLE_INTERFACE_HPP
#include "Point2D.class.hpp"

class Placeable {
public:
	virtual Point2D	getPos() const = 0;
};
#endif