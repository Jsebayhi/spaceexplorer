#ifndef PLAYER_CLASS_HPP
# define PLAYER_CLASS_HPP
# include "ToString.class.hpp"
# include "Actor.class.hpp"
# include <iostream>

class Player: public Actor{

//  CONSTRUCTOR DESTRUCTOR
public:
	Player(void);
	Player(ActorListRestricted *actorList, Point2D position);
	Player(ActorListRestricted *actorList, Point2D position, Point2D speed);
	Player(Player const & model);
	virtual ~Player(void);

//  OPERATOR
public:
	Player & operator=(Player const & rhs);

//	METHODS
public:
	void	SpeedXIncrease();
	void	SpeedXDecrease();
	void	SpeedYIncrease();
	void	SpeedYDecrease();
	void	FireWeapon();
//protected:
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	virtual std::string	formatAttributsToString(std::string separator) const;
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;

//	GETTER and SETTER

private:
	Point2D	_speedIncrement;

};
std::ostream & operator<<(std::ostream & o, Player const & rhs);

#endif