#ifndef ACTORLIST_CLASS_HPP
# define ACTORLIST_CLASS_HPP
# include "ToString.class.hpp"
# include "Actor.class.hpp"
# include "ActorListRestricted.interface.hpp"
# include <iostream>
# include <vector>

class ActorList: public ActorListRestricted, virtual public ToString {
//  STATIC METHOD
//public:
//protected:
//private:

//  VARIABLE
//public:
//protected:
	std::vector<Actor *>		_lst;
//private:

//  CONSTRUCTORS
public:
	ActorList(void);
	ActorList(ActorList const & model);
//protected:
//private:

//  DESTRUCTORS
public:
	~ActorList(void);
//protected:
//private:

//  OPERATOR
public:
	ActorList & operator=(ActorList const & rhs);
	Actor * operator[](int id);
	Actor operator[](int id) const;
//protected:
//private:

//	METHODS
public:
	bool	registerActor(Actor *actor);
	bool	removeDeadActor();
	int		getSize() const ;
//protected:
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	/* Allow to call this function to format either in inline string or as json by specifying the separator */
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, ActorList const & rhs);

#endif