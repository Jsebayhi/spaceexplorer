#ifndef ALIVE_CLASS_HPP
# define ALIVE_CLASS_HPP
# include "ToString.class.hpp"
# include <iostream>

class Alive: virtual public ToString{
//  GLOBAL METHOD
public:
	static bool isDead(const Alive * a);

//  VARIABLE
//public:
protected:
	int	_hpMin;
	int	_hpMax;
	int	_hp;
	int	_collisionDamages;
//private:


//  CONSTRUCTORS
public:
	Alive(void);
	Alive(Alive const & model);

//  DESTRUCTORS
public:
	virtual ~Alive(void);

//  OPERATOR
public:
	Alive & operator=(Alive const & rhs);

//	METHODS
public:
	virtual void ApplyDamages(int damages);
	virtual void ApplyRepair(int health);
//protected:
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	virtual std::string	formatAttributsToString(std::string separator) const;
//protected:
//private:

//  GETTER and SETTER
public:
	int	getHp() const;
	int	getHpMin() const;
	int	getHpMax() const;
	int	getCollisionDamages() const;
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, Alive const & rhs);

#endif