#ifndef ACTORLISTRESTRICTED_INTERFACE_HPP
# define ACTORLISTRESTRICTED_INTERFACE_HPP
# include "Actor.class.hpp"

class Actor;

class ActorListRestricted {
//  STATIC METHOD
//public:
//protected:
//private:

//  VARIABLE
//public:
//protected:
//private:

//  CONSTRUCTORS
//public:
//protected:
//private:

//  DESTRUCTORS
//public:
//protected:
//private:

//  OPERATOR
public:
	virtual Actor operator[](int id) const = 0;
//protected:
//private:

//	METHODS
public:
	virtual int		getSize() const = 0;
	virtual bool	registerActor(Actor *actor) = 0;
//protected:
//private:

//	UTILITIES
//public:
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};
#endif