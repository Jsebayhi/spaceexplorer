#include "Player.class.hpp"
#include <iostream>
#include <sstream>

//class Player

// public

	Player::Player(void): Actor(){
		this->_objectDefaultValues();
	}

	Player::Player(ActorListRestricted *actorList, Point2D position): Actor(actorList, position, Point2D(0, 0)){
		this->_objectDefaultValues();
	}

	Player::Player(ActorListRestricted *actorList, Point2D position, Point2D speed): Actor(actorList, position, speed){
		this->_objectDefaultValues();
	}

	Player::Player(Player const & model){
		*this = model;
	}

	Player::~Player(void){}

//  OPERATOR

	Player & Player::operator=(Player const & rhs){
		if (this != &rhs){
			Actor::operator=(rhs);
			this->_speedIncrement = rhs._speedIncrement;
		}
		return (*this);
	}

//	METHODS
//   public
	void	Player::SpeedXIncrease(){
		this->_speed.x += this->_speedIncrement.x;
	}

	void	Player::SpeedXDecrease(){
		this->_speed.x -= this->_speedIncrement.x;
	}

	void	Player::SpeedYIncrease(){
		this->_speed.y += this->_speedIncrement.y;
	}

	void	Player::SpeedYDecrease(){
		this->_speed.y -= this->_speedIncrement.y;
	}
	void	Player::FireWeapon(){
		this->_fireWeapon();
	}
//    private
	void 	Player::_objectDefaultValues(void){
		this->_speedIncrement.x = 1;
		this->_speedIncrement.y = 1;
	}

//  UTILITIES

	std::string	Player::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
		ss << Actor::formatAttributsToString(separator) << separator
			<< "_speedIncrement: " << this->_speedIncrement
		;
		return ss.str();
	}
	std::string	Player::formatToString(std::string separator) const{return ToString::formatToString(separator);}
	std::string	Player::to_string(void) const{return ToString::to_string();}
	std::string	Player::to_json(void) const{return ToString::to_json();}
//  GETTER and SETTER

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, Player const & rhs){
	o << rhs.to_string();
	return o;
}
