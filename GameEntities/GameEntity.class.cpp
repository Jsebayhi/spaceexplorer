#include "GameEntity.class.hpp"
#include <iostream>
#include <sstream>

//class GameEntity
// public

	GameEntity::GameEntity(void){
		this->_objectDefaultValues();
	}

	GameEntity::GameEntity(Point2D position, Point2D speed){
		this->_objectDefaultValues();
		this->_position = position;
		this->_speed = speed;
	}

	GameEntity::GameEntity(GameEntity const & model){
		*this = model;
	}

	GameEntity::~GameEntity(void){}

//  OPERATOR

	GameEntity & GameEntity::operator=(GameEntity const & rhs){
		if (this != &rhs){
			this->_position = rhs.getPos();
			this->_speed = rhs.getSpeed();
		}
		return (*this);
	}

//	METHODS

	void	GameEntity::Act(){
		this->_move();
	}

//  UTILITIES

	std::string	GameEntity::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
		ss << separator
			<< "_position: " << this->_position << separator
			<< "_speed: " << this->_speed 
		;
		return ss.str();
	}

//  GETTER and SETTER
	Point2D	GameEntity::getPos() const{
		return this->_position;
	}

	Point2D	GameEntity::getSpeed() const{
		return this->_speed;
	}

// private
	void	GameEntity::_objectDefaultValues(void){}


	void	GameEntity::_move(){
		this->_position.x += this->_speed.x;
		this->_position.y += this->_speed.y;
	}

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, GameEntity const & rhs){
	o << rhs.to_string();
	return o;
}