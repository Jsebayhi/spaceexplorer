#include "ActorList.class.hpp"
#include <iostream>
#include <sstream>
#include <algorithm>

//class ActorList

//  STATIC METHOD
//public
//protected
//private

//  CONSTRUCTORS
//   public
	ActorList::ActorList(void){
		this->_objectDefaultValues();
	}

	ActorList::ActorList(ActorList const & model){
		*this = model;
	}
//   protected
//   private

//  DESTRUCTORS
//   public
	ActorList::~ActorList(void){}
//   protected
//   private

//  OPERATOR
//   public
	ActorList & ActorList::operator=(ActorList const & rhs){
		if (this != &rhs){
		}
		return (*this);
	}
	Actor * ActorList::operator[](int id){
		return this->_lst[id];
	}
	Actor ActorList::operator[](int id) const{
		return *(this->_lst[id]);
	}
//   protected
//   private

//	METHODS
//   public
	bool	ActorList::registerActor(Actor *actor){
		const bool	NoError = true;

		this->_lst.push_back(actor);
		return NoError;
	}
	bool	ActorList::removeDeadActor(){
		const bool	NoError = true;

		this->_lst.erase(
				std::remove_if(this->_lst.begin(), this->_lst.end(), Alive::isDead)
				, this->_lst.end());
		return NoError;
	}
	int		ActorList::getSize() const {
		return this->_lst.size();
	}
//   protected
//   private
	void	ActorList::_objectDefaultValues(void){}

//  UTILITIES
//   public
	std::string	ActorList::formatAttributsToString(__attribute__((unused)) std::string separator) const{
		std::stringstream ss;
			ss << ""
		;
		return ss.str();
	}
	std::string	ActorList::formatToString(std::string separator) const{return ToString::formatToString(separator);}
	std::string	ActorList::to_string(void) const{return ToString::to_string();}
	std::string	ActorList::to_json(void) const{return ToString::to_json();}
//   protected
//   private

//  GETTER and SETTER
//   public
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, ActorList const & rhs){
	o << rhs.to_string();
	return o;
}