#include "Alive.class.hpp"
#include <iostream>
#include <sstream>

//class Alive
//  GLOBAL METHOD

	bool Alive::isDead(const Alive * a) {
		return a->getHp() <= 0;
	}

//  CONSTRUCTORS
//   public
	Alive::Alive(void){
		this->_objectDefaultValues();
	}
	Alive::Alive(Alive const & model){
		*this = model;
	}

//  DESTRUCTORS
//   public
	Alive::~Alive(void){}

//  OPERATOR
//   public
	Alive & Alive::operator=(Alive const & rhs){
		if (this != &rhs){
			this->_hpMin = rhs._hpMin;
			this->_hpMax = rhs._hpMax;
			this->_hp = rhs._hp;
			this->_collisionDamages = rhs._collisionDamages;
		}
		return (*this);
	}

//	METHODS
//   public
	void Alive::ApplyDamages(int damages){
		int newHp = this->_hp - damages;
		if (newHp < this->_hpMin){
			this->_hp = this->_hpMin;
		} else {
			this->_hp = newHp;
		}
	}

	void Alive::ApplyRepair(int health){
		int newHp = this->_hp + health;
		if (newHp > this->_hpMax){
			this->_hp = this->_hpMax;
		} else {
			this->_hp = newHp;
		}
	}
//   protected
//   private
	void	Alive::_objectDefaultValues(void){
		this->_hpMin = 0;
		this->_hpMax = 1;
		this->_hp = this->_hpMax;
		this->_collisionDamages = 10;
	}

//  UTILITIES
//   public
//   protected
//   private

	std::string	Alive::formatAttributsToString(std::string separator) const{
		std::stringstream ss;
			ss << "_hpMin: " << this->_hpMin << separator
			<< "_hpMax: " << this->_hpMax << separator
			<< "_hp: " << this->_hp << separator
			<< "_collisionDamages: " << this->_collisionDamages
		;
		return ss.str();
	}

//  GETTER and SETTER
//   public
	int	Alive::getHp() const {
		return this->_hp;
	}

	int	Alive::getHpMin() const {
		return this->_hpMin;
	}

	int	Alive::getHpMax() const {
		return this->_hpMax;
	}

	int	Alive::getCollisionDamages() const {
		return this->_collisionDamages;
	}
//   protected
//   private

/*
** non member
*/

std::ostream & operator<<(std::ostream & o, Alive const & rhs){
	o << rhs.to_string();
	return o;
}