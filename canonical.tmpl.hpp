#ifndef CANONICAL_HPP
# define CANONICAL_HPP
# include "ToString.class.hpp"
# include <iostream>

class Canonical: virtual public ToString{
//  STATIC METHOD
//public:
//protected:
//private:

//  VARIABLE
//public:
//protected:
//private:

//  CONSTRUCTORS
public:
	Canonical(void);
	Canonical(Canonical const & model);
//protected:
//private:

//  DESTRUCTORS
public:
	~Canonical(void);
//protected:
//private:

//  OPERATOR
public:
	Canonical & operator=(Canonical const & rhs);
//protected:
//private:

//	METHODS
//public:
//protected:
private:
	void 	_objectDefaultValues(void);

//	UTILITIES
public:
	/* Allow to call only this function when doing inheritance to avoid adress repetitions */
	virtual std::string	formatAttributsToString(std::string separator) const;
	/* Allow to call this function to format either in inline string or as json by specifying the separator */
	virtual std::string	formatToString(std::string separator) const;
	virtual std::string	to_string(void) const;
	virtual std::string	to_json(void) const;
//protected:
//private:

//  GETTER and SETTER
//public:
//protected:
//private:
};

std::ostream & operator<<(std::ostream & o, Canonical const & rhs);

#endif